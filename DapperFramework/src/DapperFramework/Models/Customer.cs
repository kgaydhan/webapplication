﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DapperFramework.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set;}

    }
}
