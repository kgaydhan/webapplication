﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Tracing;
using System.Web.Http;
using Microsoft.AspNet.Mvc.Filters;
using Microsoft.Extensions.Logging;
using DapperFramework.Logger;

namespace DapperFramework.ActionFilters
{
    public class TraceFilterAttribute : ActionFilterAttribute
    {
        private readonly ILogger ilogger;
        public TraceFilterAttribute()
        {
            ilogger = Startup.iglobalLogger;  
        }

        /// <summary>
        /// Filter Action.
        /// </summary>
        /// <param name="filterContext"></param>

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            //context.HttpContext.   
            //create mesage to log.
            var category = DateTime.Now.ToString() + " Request Start " + Environment.NewLine + "Controller : " + context.Controller.ToString() + Environment.NewLine + "Action : " + context.ActionDescriptor.DisplayName + Environment.NewLine + "Action Parameters : " + context.ActionArguments.ToJSON();
            var message = new System.Text.StringBuilder();
            message.Append(category);
            if (context.HttpContext.Request != null)
            {
                if (context.HttpContext.Request.Method != null)
                    message.Append(Environment.NewLine + "Method: " + context.HttpContext.Request.Method );

                if (context.HttpContext.Request.Path != null)
                    message.Append("").Append(Environment.NewLine + "URL: " + context.HttpContext.Request.Path);

                if (context.HttpContext.Request.Headers != null && context.HttpContext.Request.Headers.ContainsKey("Token") && context.HttpContext.Request.Headers["Token"].Any() && context.HttpContext.Request.Headers["Token"].FirstOrDefault() != null)
                    message.Append("").Append("Token: " + context.HttpContext.Request.Headers["Token"].FirstOrDefault() + Environment.NewLine);
            }

            object state = "Trace";
            ilogger.LogInformation(message.ToString());
            base.OnActionExecuting(context);
        }
       
    }
      
}
