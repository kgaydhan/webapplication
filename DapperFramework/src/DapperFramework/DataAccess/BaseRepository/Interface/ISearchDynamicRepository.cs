﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.BaseRepository.Interface
{
    /// <summary>
    ///  dynamic Result 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISearchDynamicRepository<T> where T: class 
    {
        List<dynamic> GetDynamicList(T item);
        List<dynamic> GetDynamicList(string Sql);
    }
}
