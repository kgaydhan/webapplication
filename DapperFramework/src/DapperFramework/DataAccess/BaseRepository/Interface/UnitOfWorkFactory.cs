﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.BaseRepository.Interface
{
    /// <summary>
    /// Transaction as Unit of Work actory - Get instant of IUnitofWork
    /// </summary>
   public class UnitOfWorkFactory
    {
           public static Func<IUnitOfWork> GetDefault = NotIntializedFactory;

            private static IUnitOfWork NotIntializedFactory()
            {
                throw new ApplicationException("Transaction is not intialized");
            }
    }
}
