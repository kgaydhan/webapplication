﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.BaseRepository.Interface
{
    /// <summary>
    ///  Entity Base class.
    /// </summary>
    public interface IEntityBase
    {
        int Id { get; set; }
    }
}
