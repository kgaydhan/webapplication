﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.BaseRepository.Interface
{
    /// <summary>
    ///  Repository Interface for CRUID Operation.  
    /// </summary>
    /// <typeparam name="T"></typeparam>

    public interface IRepository<T> where T : IEntityBase
    {
        void Add(T item);
        void Edit(T item);
        void Delete(T item);
        void Delete(int id);
    }
}
