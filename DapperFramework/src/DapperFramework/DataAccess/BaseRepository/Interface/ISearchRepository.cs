﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.BaseRepository.Interface
{
    public interface ISearchRepository<T> where T : class, IEntityBase 
    {
        T GetById(int id);
        List<T> GetList(T item);
    }
}
