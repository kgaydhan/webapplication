﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.BaseRepository.Interface;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Oracle.ManagedDataAccess.Client;

namespace DataAccessLayer.BaseRepository
{
    /// <summary>
    ///  Db Connection.
    /// </summary>
   public class ConnectionFactory : IConnectionFactory
    {
        private readonly IDbConnection connection;

        public ConnectionFactory(string type)
        {
           connection = GetSqlConnection();
        }

        public IDbConnection GetConnection()
        {
            return connection;
        }

        public IDbTransaction GetTransaction()
        {
            return null;
        }

        /// <summary>
        ///  Sql Connection 
        /// </summary>
        /// <returns></returns>
        public SqlConnection GetSqlConnection()
        {
            string connectionStr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            return new SqlConnection(connectionStr);
        }

        /// <summary>
        /// Oracle Connection.
        /// </summary>
        /// <returns></returns>
        public OracleConnection GetOracleConnection()
        {
            string connectionStr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            return new OracleConnection(connectionStr);
        }
    }
}
