﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.BaseRepository.Interface;
using Dapper;
using DataAccessLayer.Dapper.Wrapper;
  
namespace DataAccessLayer.BaseRepository
{
    public abstract class Repository<T> : RepositoryBase, IRepository<T> where T : class, IEntityBase
    {
        protected Repository(IConnectionFactory connectionFactory, string tableName)
            : base(connectionFactory, tableName)
        {
        }

        public void Add(T item)
        {
            dbConnection.Insert<T>(TableName, item, dbTransaction);
        }

        public void Edit(T item)
        {
            dbConnection.Edit<T>(TableName, item, dbTransaction);
        }

        public void Delete(T item)
        {
            if (item != null)
            {
                string query = String.Format("DELETE FROM [{0}] Where Id = @id", TableName);
                dbConnection.Query(query, item, dbTransaction);
            }
        }

        public void Delete(int id)
        {
            string query = String.Format("DELETE FROM [{0}] Where Id = @id", TableName);
            dbConnection.Query(query, new { id }, dbTransaction);
        }
    }
}
