﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.BaseRepository.Interface;
using Dapper;
using Oracle.ManagedDataAccess.Client;

namespace DataAccessLayer.BaseRepository
{
    /// <summary>
    ///  Repository Class will return T or List of T.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class SearchRepository<T> : RepositoryBase, ISearchRepository<T> where T : class, IEntityBase 
    {
        protected SearchRepository(IConnectionFactory connectionFactory, string tableName)
            : base(connectionFactory, tableName)
        {
        }

        public T GetById(int id)
        {
            string query = String.Format("SELECT * FROM [{0}] WHERE Id = @id", TableName);
            return dbConnection.Query<T>(query, new { id }, dbTransaction).FirstOrDefault();
        }

        /// <summary>
        /// list of Items.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public List<T> GetList(T item)
        {
            List<T> returnList = new List<T>(); 
            string query = String.Format("SELECT * FROM [{0}] WHERE Id = @id", TableName);
            IEnumerable<T> itemEnumerable = dbConnection.Query<T>(query, item, dbTransaction);
            if (itemEnumerable.Any())
            {
                returnList = itemEnumerable.ToList(); 
            }
            return returnList;
        }

        /// <summary>
        /// Dynamic Result list.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
       public List<dynamic> GetDynamicList(T item)
        {
            List<dynamic> returnList = new List<dynamic>();
            string query = String.Format("SELECT * FROM [{0}] WHERE Id = @id", TableName);
            IEnumerable<dynamic> itemEnumerable = dbConnection.Query(query, item, dbTransaction);
            if (itemEnumerable.Any())
            {
                returnList = itemEnumerable.ToList();
            }
            return returnList;
        }
    }
}
