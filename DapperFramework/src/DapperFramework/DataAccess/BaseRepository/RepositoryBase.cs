﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccessLayer.BaseRepository.Interface;

namespace DataAccessLayer.BaseRepository
{
    public abstract class RepositoryBase
    {
        protected readonly IDbConnection dbConnection;
        protected readonly IDbTransaction dbTransaction;

        protected string TableName { get; private set; }

        protected RepositoryBase(IConnectionFactory connectionFactory, string tableName)
        {
            dbConnection = connectionFactory.GetConnection();
            dbTransaction = connectionFactory.GetTransaction();
            TableName = tableName;
        }
    }
}
