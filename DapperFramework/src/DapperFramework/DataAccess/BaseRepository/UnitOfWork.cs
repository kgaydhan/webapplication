﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.BaseRepository.Interface;
using System.Data;

namespace DataAccessLayer.BaseRepository
{
    /// <summary>
    ///  class take care database Transaction  as unit.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbConnection dbConnection;
        private readonly IDbTransaction transaction;

        public UnitOfWork(IConnectionFactory connectionFactory)
        {
            dbConnection = connectionFactory.GetConnection();
            transaction = connectionFactory.GetTransaction();
        }

        public void Begin()
        {
            if (transaction == null)
                throw new InvalidOperationException("Begin method cannot call if transaction is null");
        }

        public void Commit()
        {
            transaction.Commit();
        }

        public void Rollback()
        {
            transaction.Rollback();
        }

        public void Dispose()
        {
            Rollback();
            dbConnection.Close();
        }
    }
}
