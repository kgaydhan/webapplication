﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DataAccessLayer.BaseRepository.Interface;
using DataAccessLayer.BaseRepository;
using System.Data;
using System.Reflection;

namespace DataAccessLayer.Dapper.Wrapper
{
    /// <summary>
    /// Dapper Extension Methods.
    /// </summary>
    public static class DapperExtension
    {
        /// <summary>
        /// Add and return object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection"></param>
        /// <param name="tableName"></param>
        /// <param name="item"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static T Insert<T>(this IDbConnection connection, string tableName, T item, IDbTransaction transaction = null) where T : IEntityBase 
        {
            IEnumerable<T> result = connection.Query<T>(DynamicQuery.GetInsertQuery(tableName, item), item, transaction);
            return result.First();
        }

        /// <summary>
        /// edit and return object. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection"></param>
        /// <param name="tableName"></param>
        /// <param name="item"></param>
        /// <param name="transaction"></param>
        public static void Edit<T>(this IDbConnection connection, string tableName, T item, IDbTransaction transaction = null) where T : IEntityBase
        {
          connection.Execute(DynamicQuery.GetUpdateQuery(tableName, item), item, transaction);
        }

        /// <summary>
        ///  Oracle Client Insert Method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection"></param>
        /// <param name="tableName"></param>
        /// <param name="item"></param>
        /// <param name="transaction"></param>
        /// <param name="IdentityColumn"></param>
        /// <returns></returns>

        public static T Insert<T>(this IDbConnection connection, string tableName, T item, IDbTransaction transaction = null, string IdentityColumn="") where T : IEntityBase
        {
            IEnumerable<T> result = connection.Query<T>(DynamicQuery.GetInsertQuery(tableName, item, IdentityColumn), item, transaction);
            return result.First();
        }

        /// <summary>
        /// Oracle Client Update Method.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection"></param>
        /// <param name="tableName"></param>
        /// <param name="item"></param>
        /// <param name="transaction"></param>
        public static void Edit<T>(this IDbConnection connection, string tableName, T item, IDbTransaction transaction = null, string IdentityColumn="") where T : IEntityBase
        {
            connection.Execute(DynamicQuery.GetUpdateQuery(tableName, item, IdentityColumn), item, transaction);
        }
    }

    public sealed class DynamicQuery
    {
        /// <summary>
        /// Oracle base generic query.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="item"></param>
        /// <param name="IdentityColumn"></param>
        /// <returns></returns>
        public static string GetInsertQuery<T>(string tableName, T item, string IdentityColumn) where T : IEntityBase
        {
            PropertyInfo[] props = typeof(T).GetProperties();

            string[] columns = props.Select(p => p.Name).ToArray();

            return string.Format("INSERT INTO {0}({1}) VALUES (:{2})",
                                 tableName,
                                 String.Join(",", columns),
                                 String.Join(",:", columns));
        }

        /// <summary>
        /// Oracle base Generic Query.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="item"></param>
        /// <param name="IdentityColumn"></param>
        /// <returns></returns>
        public static string GetUpdateQuery<T>(string tableName, T item, string IdentityColumn) where T : IEntityBase
        {
            PropertyInfo[] props = typeof(T).GetProperties();
            string[] columns = props.Select(p => p.Name).Where(s => s != "Id").ToArray();

            var parameters = columns.Select(name => name + "=:" + name).ToList();

            return String.Format("UPDATE {0} SET {1} WHERE {0} = :{0}",
                                 tableName,
                                 String.Join(",", parameters), IdentityColumn, IdentityColumn);
        }

        public static string GetInsertQuery<T>(string tableName, T item) where T : IEntityBase
        {
            PropertyInfo[] props = typeof(T).GetProperties();
            string[] columns = props.Select(p => p.Name).Where(s => s != "Id").ToArray();

            return string.Format("INSERT INTO [{0}]({1}) OUTPUT inserted.Id VALUES (@{2})",
                                 tableName,
                                 String.Join(",", columns),
                                 String.Join(",@", columns));
        }


        public static string GetUpdateQuery<T>(string tableName, T item) where T : IEntityBase
        {
            PropertyInfo[] props = typeof(T).GetProperties();
            string[] columns = props.Select(p => p.Name).Where(s => s != "Id").ToArray();

            var parameters = columns.Select(name => name + "=@" + name).ToList();

            return String.Format("UPDATE [{0}] SET {1} WHERE Id = @Id",
                                 tableName,
                                 String.Join(",", parameters));
        }
    }
}
