﻿
using System;
using System.Diagnostics;
using Microsoft.Extensions.Logging.TraceSource;

namespace Microsoft.Extensions.Logging
{
    public static class TraceSourceFactoryExtensions
    {
        #region "Method"
        public static ILoggerFactory AddTraceSourceExtended(this ILoggerFactory factory, SourceSwitch sourceSwitch, TraceListener listener)
        {
            if (factory == null)
            {
                throw new ArgumentNullException(nameof(factory));
            }

            if (sourceSwitch == null)
            {
                throw new ArgumentNullException(nameof(sourceSwitch));
            }

            if (listener == null)
            {
                throw new ArgumentNullException(nameof(listener));
            }

            factory.AddProvider(new TraceLoggerProvider(sourceSwitch, listener));

            return factory;
        }

        #endregion

    }
}
