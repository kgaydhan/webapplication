﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Microsoft.Extensions.Logging.TraceSource
{
    public class TraceSourceScope : IDisposable
    {
        #region "Variables"
        private bool isDisposed;
        #endregion

        #region "Method"

        public TraceSourceScope(object state)
        {
            Trace.CorrelationManager.StartLogicalOperation(state);
        }

        public void Dispose()
        {
            if (!isDisposed)
            {
                Trace.CorrelationManager.StopLogicalOperation();
                isDisposed = true;
            }
        }

        #endregion
    }

}
