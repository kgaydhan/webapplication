﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Diagnostics;
using DiagnosticsTraceSource = System.Diagnostics.TraceSource;

namespace Microsoft.Extensions.Logging.TraceSource
{
    public class TraceLoggerProvider : ILoggerProvider
    {
        #region "Variables"

        private readonly SourceSwitch LoggerRootSourceSwitch;
        private readonly TraceListener LoggerRootTraceListener;
        private readonly ConcurrentDictionary<string, DiagnosticsTraceSource> sources = new ConcurrentDictionary<string, DiagnosticsTraceSource>(StringComparer.OrdinalIgnoreCase);
        private bool isdisposed = false;

        #endregion

        #region "Method"
        /// <summary>
        /// Initializes a new instance of the <see cref="TraceSourceLoggerProvider"/> class.
        /// </summary>
        /// <param name="rootSourceSwitch"></param>
        /// <param name="rootTraceListener"></param>
        public TraceLoggerProvider(SourceSwitch rootSourceSwitch, TraceListener rootTraceListener)
        {
            if (rootSourceSwitch == null)
            {
                throw new ArgumentNullException(nameof(rootSourceSwitch));
            }

            if (rootTraceListener == null)
            {
                throw new ArgumentNullException(nameof(rootTraceListener));
            }

            LoggerRootSourceSwitch = rootSourceSwitch;
            LoggerRootTraceListener = rootTraceListener;
        }

        /// <summary>
        /// Creates a new <see cref="ILogger"/>  for the given component name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ILogger CreateLogger(string name)
        {
            return new TraceSourceLogger(GetOrAddTraceSource(name));
        }

        private DiagnosticsTraceSource GetOrAddTraceSource(string name)
        {
            return sources.GetOrAdd(name, InitializeTraceSource);
        }

        private DiagnosticsTraceSource InitializeTraceSource(string traceSourceName)
        {
            var traceSource = new DiagnosticsTraceSource(traceSourceName);
            string parentSourceName = ParentSourceName(traceSourceName);

            if (string.IsNullOrEmpty(parentSourceName))
            {
                if (HasDefaultSwitch(traceSource))
                {
                    traceSource.Switch = LoggerRootSourceSwitch;
                }

                if (LoggerRootTraceListener != null)
                {
                    traceSource.Listeners.Add(LoggerRootTraceListener);
                }
            }
            else
            {
                if (HasDefaultListeners(traceSource))
                {
                    DiagnosticsTraceSource parentTraceSource = GetOrAddTraceSource(parentSourceName);
                    traceSource.Listeners.Clear();
                    traceSource.Listeners.AddRange(parentTraceSource.Listeners);
                }

                if (HasDefaultSwitch(traceSource))
                {
                    DiagnosticsTraceSource parentTraceSource = GetOrAddTraceSource(parentSourceName);
                    traceSource.Switch = parentTraceSource.Switch;
                }
            }

            return traceSource;
        }

        private static string ParentSourceName(string traceSourceName)
        {
            int indexOfLastDot = traceSourceName.LastIndexOf('.');
            return indexOfLastDot == -1 ? null : traceSourceName.Substring(0, indexOfLastDot);
        }

        private static bool HasDefaultListeners(DiagnosticsTraceSource traceSource)
        {
            return traceSource.Listeners.Count == 1 && traceSource.Listeners[0] is DefaultTraceListener;
        }

        private static bool HasDefaultSwitch(DiagnosticsTraceSource traceSource)
        {
            return string.IsNullOrEmpty(traceSource.Switch.DisplayName) == string.IsNullOrEmpty(traceSource.Name) &&
                traceSource.Switch.Level == SourceLevels.Off;
        }

        public void Dispose()
        {
            if (!isdisposed)
            {
                isdisposed = true;
                LoggerRootTraceListener.Flush();
                LoggerRootTraceListener.Dispose();
            }
        }


        #endregion

    }
}
