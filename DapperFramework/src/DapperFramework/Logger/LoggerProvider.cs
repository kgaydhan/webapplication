﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DapperFramework.Logger
{
    public class LoggerProvider : ILoggerProvider
    {
            private readonly string loggerbasePath;

            public LoggerProvider(string basePath)
            {
                loggerbasePath = basePath;
            }

            public ILogger CreateLogger(string categoryName)
            {
                return new Logger(categoryName, loggerbasePath);
            }

            public void Dispose()
            {
            }
     
    }
}
