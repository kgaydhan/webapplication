﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using DiagnosticsTraceSource = System.Diagnostics.TraceSource;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Web.Http.Tracing;
using System.IO;

namespace Microsoft.Extensions.Logging.TraceSource
{
    public class TraceSourceLogger : ILogger
    {
        #region "Variables"
        private readonly DiagnosticsTraceSource loggerTraceSource;
        #endregion

        #region "Method"
        public TraceSourceLogger(DiagnosticsTraceSource traceSource)
        {
           loggerTraceSource = traceSource;
        }

        public IDisposable BeginScopeImpl(object state)
        {
            return new NoopDisposable();
        }

        private class NoopDisposable : IDisposable
        {
            public void Dispose()
            {
            }
        }

        public void Log(LogLevel logLevel, int eventId, object state, Exception exception, Func<object, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
            {
                return;
            }
            var message = string.Empty;
            if (formatter != null)
            {
                message = formatter(state, exception);
            }
            else
            {
                if (state != null)
                {
                    message += state;
                }
                if (exception != null)
                {
                    message += Environment.NewLine + exception;
                }
            }
            if (!string.IsNullOrEmpty(message))
            {
                lock(this)
                {
                    File.AppendAllText(DapperFramework.Startup.TraceLogPath, $"{logLevel} :: {message + Environment.NewLine}");
                } 
            }
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            var traceEventType = GetEventType(logLevel);
            return loggerTraceSource.Switch.ShouldTrace(traceEventType);
        }

        /// <summary>
        /// Write Trace.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="category"></param>
        /// <param name="level"></param>
        /// <param name="traceAction"></param>

        private static TraceEventType GetEventType(LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Critical: return TraceEventType.Critical;
                case LogLevel.Error: return TraceEventType.Error;
                case LogLevel.Warning: return TraceEventType.Warning;
                case LogLevel.Information: return TraceEventType.Information;
                default: return TraceEventType.Information;
            }
        }

        #endregion
    }
}
