﻿using System;
using Newtonsoft.Json;
using System.Data;
using System.Collections.Generic;

namespace DapperFramework.Logger
{
        public static class JSONHelper
    {
            #region Public extension methods.
            /// <summary>
            /// Extened method of object class
            /// Converts an object to a json string.
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public static string ToJSON(this object obj)
            {
                try
                {
                    return JsonConvert.SerializeObject(obj); 
                }
                catch (Exception Ex)
                {
                    return "";
                }
            }
            #endregion
        }
    }


