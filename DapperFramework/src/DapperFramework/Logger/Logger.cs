﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DapperFramework.Logger
{
    public class Logger: ILogger
    {
        private readonly string loggerCategoryName;
        private readonly string loggerPath;

        public Logger(string categoryName, string basePath)
        {
            loggerPath = Path.Combine(basePath, "mylog.txt");
            loggerCategoryName = categoryName;
        }

        public void Log(LogLevel logLevel, int eventId, object state, Exception exception,
            Func<object, Exception, string> formatter)
        {
            using (var writer = File.AppendText(loggerPath))
            {
                writer.WriteLine($"{logLevel} :: {loggerCategoryName} :: {formatter(state, exception)}");
            }
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public IDisposable BeginScopeImpl(object state)
        {
            return new NoopDisposable();
        }

        private class NoopDisposable : IDisposable
        {
            public void Dispose()
            {
            }
        }
    }
}
