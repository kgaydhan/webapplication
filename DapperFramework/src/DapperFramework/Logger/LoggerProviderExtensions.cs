﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace DapperFramework.Logger
{
    public static class LoggerProviderExtensions
    {
        public static ILoggerFactory AddLogger(this ILoggerFactory loggerFactory, string basePath)
        {
            loggerFactory.AddProvider(new LoggerProvider(basePath));

            return loggerFactory;
        }
    }
}
