﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Http;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
   
namespace DapperFramework.Handlers
{
    public class Middleware
    {
        RequestDelegate nextAction;
        ILogger ilogger;
        public Middleware(RequestDelegate next)
        {
            nextAction = next;
            ilogger = Startup.iglobalLogger;  
        }

        /// <summary>
        ///  Invoke method
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            var bodyStream = context.Response.Body;

            var responseBodyStream = new MemoryStream();
            context.Response.Body = responseBodyStream;

            await nextAction(context);

            responseBodyStream.Seek(0, SeekOrigin.Begin);
            var responseBody = new StreamReader(responseBodyStream).ReadToEnd();
            if (responseBody.Length > 0)
            {
                ilogger.LogInformation($" Response : {responseBody}" + System.Environment.NewLine );
            }
            responseBodyStream.Seek(0, SeekOrigin.Begin);
            await responseBodyStream.CopyToAsync(bodyStream);
        }
    }
}
